#include "io.h"
#include "qubobus.h"

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

char *whoami;

/* Print hex of bytes in buffer separated by spaces */
void debug_print_buf(void *buffer, size_t size) {
    int i;

    printf("%s:", whoami);
    for (i = 0; i < size; i++) {
        printf(" %02x", ((uint8_t *)buffer)[i]);
    }
    printf("\n");
}

/*** `raw_io_function`s ***/

ssize_t raw_read(void *io_host, void *buffer, size_t size) {
    return read(((int *)io_host)[0], buffer, size);
}

ssize_t raw_write(void *io_host, void *buffer, size_t size) {
    debug_print_buf(buffer, size);
    return write(((int *)io_host)[1], buffer, size);
}

int main(int argc, char *argv[]) {
    int i;
    /* rx_fd means "receive", tx_fd means "transmit". Modeled after UART.
     * These are file descriptors returned by `open`.*/
    int rx_fd, tx_fd;
    /* `raw_io_function`s and the like only pass around a single variable
     * "io_host", which includes the ability for both read and write, so we
     * need an array containing both Rx and Tx. */
    int pipe[2];
    int error;
    uint8_t buffer[32 * sizeof(uint8_t)];
    IO_State state;

    whoami = argv[0];

    /* process command line arguments */
    if (argc != 3) {
        fprintf(stderr, "usage: %s READ_FIFO WRITE_FIFO\n", argv[0]);
        exit(-1);
    }
    /* argument: WRITE_FIFO
     *
     * `access()` checks that a file exists. We do this because when
     * opening a file for writing, it will automatically be created if it
     * does not exist. However we do not want this because we want the file
     * to be a named pipe (created with `mkfifo`) that should already exist
     * when this program is called */
    if (access(argv[2], F_OK) != 0) { /* if error not zero */
        fprintf(stderr, "WRITE_FIFO \"%s\" not found\n", argv[2]);
        exit(-1);
    }
    printf("opening WRITE_FIFO \"%s\"\n", argv[2]);
    if ((tx_fd = open(argv[2], O_WRONLY)) < 0) {
        fprintf(stderr, "cannot open WRITE_FIFO \"%s\" for writing\n",
                argv[2]);
        exit(-1);
    }

    /* argument: READ_FIFO */
    printf("opening READ_FIFO \"%s\"\n", argv[1]);
    if ((rx_fd = open(argv[1], O_RDONLY)) < 0) {
        fprintf(stderr, "cannot open READ_FIFO \"%s\" for reading\n",
                argv[1]);
        exit(-1);
    }

    pipe[0] = rx_fd;
    pipe[1] = tx_fd;
    /* Last argument is "priority", and `state.local_sequence_number` is
     * initialized with it. I'm just copying what was used in `test_io.c`.
     * */
    state = initialize(&pipe, &raw_read, &raw_write, 80);
    printf("%s: wait_connect ...\n", argv[0]);
    error = wait_connect(&state, buffer);

    {
        struct Depth_Status depth_status = {3.14f, 2};
        Message m = create_response(&tDepthStatus, &depth_status);
        write_message(&state, &m);
    }
    if (error != 0) {
        printf("%s: error code %d\n", argv[0], error);
    } else {
        printf("%s: Success.\n", argv[0]);
    }

    /* for (i = 0; i < 5; i++) { */
    /*  printf("tiva\n"); */
    /*  usleep(1000000 * 5); */
    /* } */
    close(rx_fd);
    close(tx_fd);
    return 0;
}
