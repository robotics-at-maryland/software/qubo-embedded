# Ross Baeh
# R@M 2017
# ross.baehr@gmail.com

# To add a new module to this makefile you need to do the following steps:
#   1. Add the module to the SRC_C_FILES variable
#   2. Make sure the module is created in the objects folder with the $(OBJDIR) target command
#   3. Make sure the module is added to the clean_obj PHONY target
#   
# And that should do it!

# FreeRTOS and src objects are put into obj/
# Tivaware objects are built in drivers/ and symlinked into obj/

LINKER_SCRIPT = $(SRC)tiva.ld
ELF_IMAGE = image.elf
TARGET = image.bin

# You shouldn't need to edit anything below here
#########################################################################
# FLAGS                                                                 #
#########################################################################
TOOLCHAIN = arm-none-eabi-
CC = $(TOOLCHAIN)gcc
CXX = $(TOOLCHAIN)g++
AS = $(TOOLCHAIN)as
LD = $(TOOLCHAIN)ld
OBJCOPY = $(TOOLCHAIN)objcopy
AR = $(TOOLCHAIN)gcc-ar

# GCC flags
#
CFLAG = -c
OFLAG = -o
INCLUDEFLAG = -I

CFLAGS =  -g -mthumb -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard
CFLAGS += -Os -ffunction-sections -fdata-sections -MD -std=c99
CFLAGS += -Werror -pedantic -DPART_TM4C123GH6PM -c 
CFLAGS += -DTARGET_IS_TM4C123_RB2
CFLAGS += -Dgcc

LTO_PLUGIN = $(shell arm-none-eabi-gcc --print-file-name=liblto_plugin.so)
LDFLAGS = -T $(LINKER_SCRIPT) --entry ResetISR --gc-sections 

# Default debug off unless ran with make debug
DEBUG = OFF

##########################################################################
# LOCATIONS                                                              #
##########################################################################

# Directory with HW drivers' source files
DRIVERS_SRC = drivers/

# Directory with demo specific source (and header) files
SRC = src/

# Intermediate directory for all *.o and other files:
OBJDIR = obj/

# These objects are needed for linking
LIBGCC = $(shell $(CC) $(CFLAGS) -print-libgcc-file-name)
LIBC = $(shell $(CC) $(CFLAGS) -print-file-name=libc.a)
LIBM = $(shell $(CC) $(CFLAGS) -print-file-name=libm.a)

# Additional C compiler flags to produce debugging symbols
DEB_FLAG = -g -DDEBUG

# Compiler/target path in FreeRTOS/Source/portable
PORT_COMP_TARG = GCC/ARM_CM4F/

# FreeRTOS source base directory
FREERTOS_SRC = FreeRTOS/Source/

# Directory with memory management source files
FREERTOS_MEMMANG_SRC = $(FREERTOS_SRC)portable/MemMang/

# Directory with platform specific source files
FREERTOS_PORT_SRC = $(FREERTOS_SRC)portable/$(PORT_COMP_TARG)

# Qubobus src files
QUBOBUS_SRC = qubobus/src/

# Thruster Source
THRUSTERS_SRC = src/thrusters/

##########################################################################
# OBJECTS                                                                #
##########################################################################

# Object files for FreeRTOS
FREERTOS_OBJS = queue.o list.o tasks.o stream_buffer.o
# The following o. files are only necessary if
# certain options are enabled in FreeRTOSConfig.h
#FREERTOS_OBJS += timers.o
#FREERTOS_OBJS += croutine.o
#FREERTOS_OBJS += event_groups.o

# Only one memory management .o file must be uncommented!
#FREERTOS_MEMMANG_OBJS = heap_1.o
#FREERTOS_MEMMANG_OBJS = heap_2.o
#FREERTOS_MEMMANG_OBJS = heap_3.o
FREERTOS_MEMMANG_OBJS = heap_4.o
#FREERTOS_MEMMANG_OBJS = heap_5.o

FREERTOS_PORT_OBJS = port.o

# Driver object lists to tivaware libs
DRIVERLIB_OBJS = $(wildcard $(OBJDIR)driverlib/*.o)
DRIVERLIB_OBJS := $(DRIVERLIB_OBJS) $(wildcard $(OBJDIR)driverlib/*.a)
UTILS_OBJS = $(wildcard $(OBJDIR)utils/*.o)

#USBLIB_OBJS = $(wildcard $(OBJDIR)usblib/*.o)

TIVA_DRIVER_OBJS = $(DRIVERLIB_OBJS)

# List of source file objects.
SRC_C_FILES = $(wildcard $(SRC)*.c) $(wildcard $(SRC)thrusters/*.c) \
			  $(wildcard $(SRC)cli/*.c) \
			  $(wildcard $(SRC)config/*.c) \
			  $(wildcard $(SRC)qubobus/*.c)
SRC_OBJS := $(SRC_C_FILES:$(SRC)%=%)
SRC_OBJS := $(SRC_OBJS:.c=.o)

QUBOBUS_OBJECTS = io.o protocol.o embedded.o safety.o battery.o power.o thruster.o pneumatics.o depth.o debug.o

# All object files specified above are prefixed the object directory
OBJS = $(addprefix $(OBJDIR), $(FREERTOS_OBJS) $(FREERTOS_MEMMANG_OBJS) $(FREERTOS_PORT_OBJS) \
	$(SRC_OBJS) $(QUBOBUS_OBJECTS))
OBJS += $(DRIVERLIB_OBJS)

##########################################################################
# INCLUDES                                                               #
##########################################################################
# FreeRTOS core include files
INC_FREERTOS = $(FREERTOS_SRC)include/

INC_QUBOBUS = qubobus/include/

# Complete include flags to be passed to $(CC) where necessary
INC_FLAGS = $(addprefix $(INCLUDEFLAG), $(INC_FREERTOS) $(FREERTOS_PORT_SRC) \
			$(DRIVERS_SRC) \
			$(SRC) \
			$(THRUSTERS_SRC) \
			$(SRC)cli/ \
			$(SRC)config/ \
			$(SRC)qubobus/ \
			$(INC_QUBOBUS))

DEP_FRTOS_CONFIG = $(SRC)FreeRTOSConfig.h

##########################################################################
# RULES                                                                  #
##########################################################################

all : $(TARGET)

debug: clean debug_flag all

debug_flag: tiva
	$(eval DEBUG := ON)
	$(eval CFLAGS += $(DEB_FLAG))

tiva:
	bash -c "cd drivers;./symlink_objs &> /dev/null"

rebuild : clean

setenv:
	source ./scripts/setenv.sh

dbgrun:
	./scripts/start_openocd.sh

dbg:
	arm-none-eabi-gdb $(ELF_IMAGE)

$(TARGET) : $(OBJDIR) $(ELF_IMAGE)
	$(OBJCOPY) -O binary $(word 2,$^) $@ && ./scripts/static_usage.sh image.elf

$(OBJDIR) :
	mkdir -p $@ $(OBJDIR)lib/ $(OBJDIR)thrusters/ $(OBJDIR)cli/ $(OBJDIR)/config $(OBJDIR)/qubobus/

$(ELF_IMAGE) : $(OBJS) $(LIBC) $(LIBM) $(LIBGCC)
	@if [ $(DEBUG) = "ON" ]; then \
		echo "_____________________________________________________________";\
		echo "Debug on";\
		echo "$(LD) $(OFLAG) $@ $^ $(UTILS_OBJS) $(LDFLAGS)";\
		$(LD) $(OFLAG) $@ $^ $(UTILS_OBJS) $(LDFLAGS);\
	else \
		echo "_____________________________________________________________";\
		echo "Debug off";\
		echo "$(LD) $(OFLAG) $@ $^ $(UTILS_OBJS) $(LDFLAGS)";\
		$(LD) $(OFLAG) $@ $^ $(UTILS_OBJS) $(LDFLAGS);\
	fi

$(OBJDIR)%.o: $(SRC)%.c $(OBJDIR)
	$(CC) $(CFLAG) $(CFLAGS) $(INC_FLAGS) $< $(OFLAG) $@

# Make objects for the FreeRTOS Files
$(OBJDIR)%.o: $(FREERTOS_SRC)%.c $(INC_FREERTOS)* $(DEP_FRTOS_CONFIG)
	$(CC) $(CFLAG) $(CFLAGS) $(INC_FLAGS) $< $(OFLAG) $@

$(OBJDIR)%.o: $(FREERTOS_MEMMANG_SRC)%.c $(DEP_FRTOS_CONFIG)
	$(CC) $(CFLAG) $(CFLAGS) $(INC_FLAGS) $< $(OFLAG) $@

$(OBJDIR)%.o: $(FREERTOS_PORT_SRC)%.c $(DEP_FRTOS_CONFIG)
	$(CC) $(CFLAG) $(CFLAGS) $(INC_FLAGS) $< $(OFLAG) $@

$(OBJDIR)%.o: $(QUBOBUS_SRC)%.c $(INC_QUBOBUS)*
	$(CC) $(CFLAG) $(CFLAGS) $(INC_FLAGS) $< $(OFLAG) $@

usage:
	./utils/static_usage.bash image.elf

drivers:
	cd drivers && make

# Cleanup directives:

clean_obj :
	$(RM) -rf $(OBJDIR)*.o $(OBJDIR)*.d \
		$(OBJDIR)thrusters \
		$(OBJDIR)cli/ \
		$(OBJDIR)config/

clean_intermediate : clean_obj
	$(RM) *.elf
	$(RM) *.img

clean : clean_intermediate
	$(RM) *.bin

flash:
	sudo /usr/bin/lm4flash ./image.bin

# Short help instructions:
print-%  : ; @echo $* = $($*)

# Automatically format code
format:
	./scripts/format.sh

.PHONY :  all rebuild clean clean_intermediate clean_obj debug debug_rebuild _debug_flags help dbgrun dbg setenv $(OBJDIR) format
