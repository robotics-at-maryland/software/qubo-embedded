/* Nathaniel Renegar
   R@M 2021
   naterenegar@gmail.com
*/

/**** Module Includes ****/
#include "cli.h"
#include "thrusters.h" // needed for the thrus_data struct
#include "qubo-utils.h"

/**** Standard Includes ****/
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

/**** Driver Includes ****/
#include <inc/hw_memmap.h>
#include <driverlib/uart.h>
#include <utils/uartstdio.h>

/**** FreeRTOS Includes ****/
#include "FreeRTOS.h"
#include "task.h"
#include "message_buffer.h"
#include "queue.h"

/**** Private Constants ****/
#define CARRIAGE_RETURN 13
#define NEWLINE         10
#define BACKSPACE       8
#define SPACE           32

#define MESSAGE_BUF_SIZE 256

static const uint32_t all_interrupts = 0xFFFFFFFF;
static const char *prompt = "tiva-cli> ";

/**** Private Datatypes ****/
struct command_t {
    char name[32];
    int  nargs;
};

enum command_names {
    HELP = 0,
    THRUSTER = 1,

    NUM_CMDS
};

/**** Private Variables ****/
static uint32_t g_cli_uart_base = UART2_BASE;
static bool initialized = false;
static char curr_command[256];
static int curr_command_len = 0;

static MessageBufferHandle_t xcli_command_buf = NULL;
static QueueHandle_t thruster_force_queue;
static QueueHandle_t thruster_pwm_queue;
static thrusparam_t thrus_data;

static struct command_t command_table[NUM_CMDS] = {
    [HELP] = {
        .name = "help",
        .nargs = 0,
    },

};

/**** Private Prototypes ****/
static void process_commands(void *params);
static void parse_command(char *cmd);

/**** Private Functions ****/
static void parse_command(char *cmd) {
    const char *thr = "ts ";
    const char *thr_force = "tf ";
    const char *thr_force_get = "fg";

    if (strncmp(cmd, thr, strlen(thr)) == 0) {
        char *thr_id = cmd + strlen(thr);

        int id, val;
        id = atoi(thr_id);
        while (*thr_id != ' ' && *thr_id != 0) {
            thr_id++;
        }
        val = atoi(thr_id);

        DEBUG_PRINTF("<cli>: Got thruster command for thruster %d and pwm %d\n", id, val);
        if ((id >= 0) && (id < 8)) {
            int64_t thruster_raw;
            ((int *) &thruster_raw)[0] = id;
            ((int *) &thruster_raw)[1] = val;
            xQueueSend(thruster_pwm_queue, (void *) &thruster_raw, 0);
        }
    } else if (strncmp(cmd, thr_force, strlen(thr_force)) == 0) {
        char *force_vec_str = cmd + strlen(thr_force);

        int idx;
        int force_int;
        float force;

        idx = atoi(force_vec_str);
        while (*force_vec_str != ' ' && *force_vec_str != 0) {
            force_vec_str++;
        }
        force_int = atoi(force_vec_str);
        force = ((float) force_int) / 1000;

        DEBUG_PRINTF("<cli>: Got thruster force update for index %d and force %d (mN)\n", idx, force_int);

        if ((idx >= 0) && (idx <= 5)) {
            float *thrust_vec = (float *) &thrus_data;
            thrust_vec[idx] = force;
            xQueueSend(thruster_force_queue, (void *) &thrus_data, 0);

            DEBUG_PRINTF("\tv[%d] = %d (mN)\n", idx, force_int);
            DEBUG_PRINTF("New thrust vector:\n");
            DEBUG_PRINTF("\tv[0] = %d (mN)\n", (int) (thrus_data.val[0] * 1000));
            DEBUG_PRINTF("\tv[1] = %d (mN)\n", (int) (thrus_data.val[1] * 1000));
            DEBUG_PRINTF("\tv[2] = %d (mN)\n", (int) (thrus_data.val[2] * 1000));
            DEBUG_PRINTF("\tv[3] = %d (mN)\n", (int) (thrus_data.val[3] * 1000));
            DEBUG_PRINTF("\tv[4] = %d (mN)\n", (int) (thrus_data.val[4] * 1000));
            DEBUG_PRINTF("\tv[5] = %d (mN)\n", (int) (thrus_data.val[5] * 1000));
        } else {
            DEBUG_PRINTF("Invalid index. Should be integer in 0 to 5\n");
        }
    } else if (strncmp(cmd, thr_force_get, strlen(thr_force_get)) == 0) {
        DEBUG_PRINTF("\nCurrent thrust vector:\n");
        DEBUG_PRINTF("\tv[0] = %d (mN)\n", (int) (thrus_data.val[0] * 1000));
        DEBUG_PRINTF("\tv[1] = %d (mN)\n", (int) (thrus_data.val[1] * 1000));
        DEBUG_PRINTF("\tv[2] = %d (mN)\n", (int) (thrus_data.val[2] * 1000));
        DEBUG_PRINTF("\tv[3] = %d (mN)\n", (int) (thrus_data.val[3] * 1000));
        DEBUG_PRINTF("\tv[4] = %d (mN)\n", (int) (thrus_data.val[4] * 1000));
        DEBUG_PRINTF("\tv[5] = %d (mN)\n", (int) (thrus_data.val[5] * 1000));
    }

    return;
}

// We could also use this for aribtrary data packets instead of just ascii
// strings. This would be useful for setting up the Tiva to Jetson link
static void process_commands(void *params) {
    char cmd[256];

    // This is the force queue we will send
    thruster_force_queue = *((QueueHandle_t *) ((void **) params)[0]);

    // This is the PWM queue we will send
    thruster_pwm_queue = *((QueueHandle_t *) ((void **) params)[1]);

    thrus_data.val[0] = 0.0;
    thrus_data.val[1] = 0.0;
    thrus_data.val[2] = 0.0;
    thrus_data.val[3] = 0.0;
    thrus_data.val[4] = 0.0;
    thrus_data.val[5] = 0.0;

    xQueueSend(thruster_force_queue, (void *) &thrus_data, 0);

    while (1) {
        // Recieve a command from some datastructure
        size_t bytes = xMessageBufferReceive(xcli_command_buf, (void *)cmd, sizeof(cmd),
                                             portMAX_DELAY);

        if (bytes > 1) {
            parse_command(cmd);
            DEBUG_PRINTF("\n");
        }
        DEBUG_PRINTF(prompt);
    }

    return;
}

/**** Public Functions ****/
bool CLI_task_init(uint32_t uart_base_addr, void *params) {
    initialized = true;
    g_cli_uart_base = uart_base_addr;
    memset(curr_command, 0, 256);

    xcli_command_buf = xMessageBufferCreate(MESSAGE_BUF_SIZE);
    if (!xcli_command_buf) {
        return false;
    }

    UARTIntRegister(g_cli_uart_base, CLIUartHandler);
    UARTIntEnable(g_cli_uart_base, UART_INT_RX | UART_INT_RT);

    DEBUG_PRINTF(prompt);

    if (xTaskCreate(process_commands, (const portCHAR *)"CLI", 256, params,
                    tskIDLE_PRIORITY, NULL) != pdTRUE) {
        return true;
    }
    return false;
}


void CLI_printf(const char *message) {
    DEBUG_PRINTF("\n");
    DEBUG_PRINTF(message);
    DEBUG_PRINTF(prompt);
}

void CLIUartHandler(void) {
    // TODO: Should probably check the flags to make sure nothing went wrong
    //uint32_t interrupt_flags = UARTIntStatus(g_cli_uart_base, all_interrupts);
    UARTIntClear(g_cli_uart_base, all_interrupts);

    while (UARTCharsAvail(g_cli_uart_base)) {
        /* Make sure we actually got a character */
        uint32_t user_input = UARTCharGetNonBlocking(g_cli_uart_base);
        char input_char;
        if (user_input == -1) {
            break;
        }
        input_char = (char) user_input;

        /* Update our command based on the character. Three possibilites:
         *   1. Entering the command
         *   2. Taking away a character
         *   3. Adding a character
         */

        /* Entering the command */
        if (input_char == CARRIAGE_RETURN) {
            UARTCharPut(g_cli_uart_base, NEWLINE);
            UARTCharPut(g_cli_uart_base, CARRIAGE_RETURN);

            curr_command[curr_command_len] = 0;
            BaseType_t higher_prio_woken;
            if (xcli_command_buf) {
                xMessageBufferSendFromISR(xcli_command_buf, (const void *)
                                          curr_command, curr_command_len + 1, &higher_prio_woken);
            }

            curr_command_len = 0;
        }
        /* Taking away a character */
        else if (input_char == BACKSPACE && curr_command_len > 0) {
            /* Handle the terminal */
            UARTCharPut(g_cli_uart_base, BACKSPACE);
            UARTCharPut(g_cli_uart_base, SPACE);
            UARTCharPut(g_cli_uart_base, BACKSPACE);

            /* Handle the command */
            curr_command[--curr_command_len] = 0;
        }
        /* Adding a character */
        else if (input_char != BACKSPACE) {
            UARTCharPut(g_cli_uart_base, input_char);
            curr_command[curr_command_len++] = input_char;
        }
    }

    return;
}
