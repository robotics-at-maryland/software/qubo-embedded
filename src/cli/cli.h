/* Nathaniel Renegar
   R@M 2021
   naterenegar@gmail.com
*/

/**** Module Includes ****/

/**** Standard Includes ****/
#include <stdint.h>
#include <stdbool.h>

/**** Driver Includes ****/

/**** FreeRTOS Includes ****/

/**** Public Constants ****/

/**** Public Datatypes ****/

/**** Public Prototypes ****/
bool CLI_task_init(uint32_t uart_base_addr, void *params);
void CLI_printf(const char *message);
void CLIUartHandler(void);
