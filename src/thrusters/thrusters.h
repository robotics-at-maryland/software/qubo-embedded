#ifndef __THRUSTERS_H_
#define __THRUSTERS_H_

#include <stdbool.h>

/* Times in us */
#define MAX_PULSE_WIDTH 1900
#define MIN_PULSE_WIDTH 1100
#define ZERO_THROTTLE_WIDTH 1500

/* These are determined from the clocking settings of the tiva and desired pwm frequency
 * See the file "src/configure.c" */
#define TIVA_PWM_TICKS 31250
#define TIVA_PWM_PERIOD 5000.0

typedef struct thruster_paramter {

    float val[6];
    int type;

} thrusparam_t;

void thruster_task(void *);

#endif /* __THRUSTERS_H_ */
