/**** Module includes ****/
#include "thrusters.h"
#include "cli.h"
#include "qubo-utils.h"

#include <stdbool.h>
#include <stdint.h>
#include <math.h>

/**** Tivalib inclues ****/
#include <inc/hw_memmap.h>
#include <driverlib/rom.h>
#include <driverlib/pwm.h>

#include <driverlib/uart.h>
#include <utils/uartstdio.h>

/**** FreeRTOS ****/
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <message_buffer.h>

/* thrusters.c: Handles the receiving of thruster data from Jetson and writes it to the
 * thrusters.
 *
 * Main function creates the initial queue for holding the raw data, it is then passed to
 * thrusters.c via RTOS params.
 * After going into hover, and after running vTaskDelay, we check to see if anything has been added
 * to the queue:
 *   - if nothing, then thrusters goes into the idle state
 *   - we otherwise apply what is in the queue to the particular thruster
 */

#define MATRIX_ROWS 8
#define MATRIX_COLS 6

/**** Private Prototypes ****/
static inline uint32_t tiva_throttle_scale(float throttle);
static inline uint32_t tiva_pwm_scale(uint32_t pulse_width);
static void set_def_state(void);
static void matrix_multiply(int, int, int, int, float *, float *, float *);
static void force_to_pwm(int, float *);
static void debug_thruster_test(QueueHandle_t, thrusparam_t *);

/**** Private Functions ****/

static inline uint32_t tiva_throttle_scale(float throttle) {
    if (throttle > 1.0) {
        throttle = 1.0;
    } else if (throttle < -1.0) {
        throttle = -1.0;
    }

    return (uint32_t) (((throttle * ((MAX_PULSE_WIDTH - MIN_PULSE_WIDTH) / 2))
                        / TIVA_PWM_PERIOD) * TIVA_PWM_TICKS);
}

static void set_def_state(void) {
    ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_0, tiva_throttle_scale(0));
    ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_1, tiva_throttle_scale(1));
    ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_2, tiva_throttle_scale(0.8));
    ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_3, tiva_throttle_scale(0.6));
    ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_4, tiva_throttle_scale(-0.2));
    ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_5, tiva_throttle_scale(-0.4));
    ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_6, tiva_throttle_scale(-0.6));
    ROM_PWMPulseWidthSet(PWM1_BASE, PWM_OUT_7, tiva_throttle_scale(-0.8));
}

/* Function to multiply matrices (note that they must be 1-D arrays) together */
static void matrix_multiply(int m1, int n1, int m2, int n2, float *matrix1,
                            float *matrix2, float *result) {
    int row, col, i;
    float sum;

    for (row = 0; row < m1; row++)
        for (col = 0; col < n2; col++) {
            sum = 0.0f;
            for (i = 0; i < n1; i++) {
                sum += matrix1[row * n1 + i] * matrix2[i * n2 + col];
            }
            result[row * n2 + col] = sum;
        }
}




/* Convert the data we receive in "force units", and convert it to "pwm units". */
static void force_to_pwm(int voltage, float *output_buffer) {
    int i;
    float force = 0.0f;

    for (i = 0; i < 8; i++) {
        force = output_buffer[i];

        if (force > 0)
            if (voltage < 12)
                force = (-1.3 + sqrt(7.44 * force + 0.8258)) / 3.72;
            else if (voltage < 14)
                force = (-1.62 + sqrt(9.48 * force + 3.8094)) / 4.74;
            else if (voltage < 16)
                force = (-1.95 + sqrt(11.6 * force + 5.4149)) / 5.8;
            else if (voltage < 18)
                force = (-2.3 + sqrt(13.2 * force + 7.3492)) / 6.6;
            else if (voltage < 20)
                force = (-1.68 + sqrt(18.4 * force + 4.41952)) / 9.2;
            else /*voltage >= 20*/
                force = (-0.822 + sqrt(24.1 * force + 0.720454)) / 12.1;
        else if (force < 0)
            if (voltage < 12)
                force = (-1.08 + sqrt(1.708404 - 5.64 * force)) / -2.82;
            else if (voltage < 14)
                force = (-1.41 + sqrt(2.78838 - 6.84 * force)) / -3.42;
            else if (voltage < 16)
                force = (-1.72 + sqrt(4.05964 - 8.28 * force)) / -4.14;
            else if (voltage < 18)
                force = (-1.95 + sqrt(5.15914 - 9.76 * force)) / -4.88;
            else if (voltage < 20)
                force = (-1.86 + sqrt(4.88504 - 12.08 * force)) / -6.04;
            else
                force = (-1.31 + sqrt(2.614564 - 15.68 * force)) / -7.84;
        else
            force = 0;

        output_buffer[i] = 1500.0 + 400.0 * force;
    }
}

// if force is negative, apply reverse

static inline uint32_t tiva_pwm_scale(uint32_t pulse_width) {
    if (pulse_width > MAX_PULSE_WIDTH) {
        pulse_width = MAX_PULSE_WIDTH;
    } else if (pulse_width < MIN_PULSE_WIDTH) {
        pulse_width = MIN_PULSE_WIDTH;
    }

    return (uint32_t) ((pulse_width / TIVA_PWM_PERIOD) * TIVA_PWM_TICKS);
}


static void debug_thruster_test(QueueHandle_t queue, thrusparam_t *data) {
    xQueueSendToBack(queue, data, portMAX_DELAY);
}

/* Activitates all the PWM outputs and fulfills the task of updating the thrusters based on what
 * Jetson requests via qubobus.
 */
/*
static uint32_t pwm_out[8] = { PWM_OUT_0,
                               PWM_OUT_1,
                               PWM_OUT_2,
                               PWM_OUT_3,
                               PWM_OUT_4,
                               PWM_OUT_5,
                               PWM_OUT_6,
                               PWM_OUT_7 };
*/
static uint32_t pwm_out[8] = { PWM_OUT_5,
                               PWM_OUT_3,
                               PWM_OUT_7,
                               PWM_OUT_1,
                               PWM_OUT_4,
                               PWM_OUT_0,
                               PWM_OUT_6,
                               PWM_OUT_2
                             };

void thruster_task(void *params) {
    /* bool init = false; */
    /* volatile int i = 0; */
    /* volatile int j = 0; */
    const TickType_t xDelay = 100 / portTICK_PERIOD_MS;
    float output_buffer[8];
    thrusparam_t thrus_data;
    thrusparam_t test_value = {{1, 1, 1, 1, 1, 1}, 1};
    QueueHandle_t xForceQueue;
    QueueHandle_t xPWMQueue; /* This is for debugging from the CLI */
    int i;
    int64_t thruster_raw = 0;
    int thruster_id = 0;
    int pwm = 0;
    /*float jetson_matrix[MATRIX_ROWS * MATRIX_COLS] = {
        0.707106781186865, -0.707106781186231, 0, 0.116672618895728, 0.116672618895833, -0.452548339959467,
        0.707106781186865, 0.707106781186231, 0, -0.116672618895728, 0.116672618895833, 0.162634559672852,
        -0.70710678118489, 0.707106781188206, 0, -0.116672618896054, -0.116672618895507, -0.162634559673188,
        -0.70710678118489, -0.707106781188206, 0, 0.116672618896054, -0.116672618895507, 0.452548339958993,
        0, 0, -1, -0.455, 0.105000000000392, 0,
        0, 0, -1, 0.05, 0.105000000000392, 0,
        0, 0, -1, 0.05, -0.104999999999608, 0,
        0, 0, -1, -0.455, -0.104999999999608, 0
    };*/

    float jetson_matrix[MATRIX_ROWS * MATRIX_COLS] = {
    0.70710678,           0,   0,   0,   0,  -0.615182,
             0,  0.70710678,   0,   0,   0,   0,
             0,           0,   0,   0,   0,  -0.615182,
   -0.70710678, -0.70710678,   0,   0,   0,   0,
             0,           0,   0,  -0.5, 0.5, 0,
             0,           0,-0.5,   0.5, 0.5, 0, 
             0,           0,   0,   0.5,-0.5, 0,
             0,           0,-0.5,  -0.5,-0.5, 0 
    };

    xForceQueue = *((QueueHandle_t *) ((void **) params)[0]);
    xPWMQueue = *((QueueHandle_t *) ((void **) params)[1]);
    DEBUG_PRINTF("inside thrusters task\n");

    if (xQueueReceive(xForceQueue, &thrus_data, pdMS_TO_TICKS(1000)) == pdPASS) {
        DEBUG_PRINTF("There was something in the queue\n");
        DEBUG_PRINTF("%d\n", (int) thrus_data.val[0]);
        DEBUG_PRINTF("%d\n", (int) thrus_data.val[1]);
        DEBUG_PRINTF("%d\n", (int)thrus_data.val[2]);
        DEBUG_PRINTF("%d\n", (int)thrus_data.val[3]);
        DEBUG_PRINTF("%d\n", (int)thrus_data.val[4]);
        DEBUG_PRINTF("%d\n", (int)thrus_data.val[5]);
    } else {
        DEBUG_PRINTF("I am false\n");
    }

    DEBUG_PRINTF("Test Values\n");
    DEBUG_PRINTF("%d\n", test_value.val[0]);
    DEBUG_PRINTF("%d\n", test_value.val[1]);
    DEBUG_PRINTF("%d\n", test_value.val[2]);
    DEBUG_PRINTF("%d\n", test_value.val[3]);
    DEBUG_PRINTF("%d\n", test_value.val[4]);
    DEBUG_PRINTF("%d\n", test_value.val[5]);

    /*xPWMQueue = *((QueueHandle_t **) params)[1];*/

    // PWM is configured to have period of 5000 us (freq 200 Hz) divided into 31250 ticks
    // (1500 / 5000) * 31250 = 9375 (see configure.c)

    set_def_state();        // New function to set def state
    // With PWM period, ticks, and pulse width set, we're ready to enable the generators
    ROM_PWMGenEnable(PWM1_BASE, PWM_GEN_0);
    ROM_PWMGenEnable(PWM1_BASE, PWM_GEN_1);
    ROM_PWMGenEnable(PWM1_BASE, PWM_GEN_2);
    ROM_PWMGenEnable(PWM1_BASE, PWM_GEN_3);

    for (i = 0; i < 8; i++) {
        ROM_PWMPulseWidthSet(PWM1_BASE, pwm_out[i], tiva_pwm_scale(1500));
    }

    // Now enable the outputs
    ROM_PWMOutputState(PWM1_BASE, PWM_OUT_0_BIT | PWM_OUT_1_BIT
                       | PWM_OUT_2_BIT | PWM_OUT_3_BIT, true);
    ROM_PWMOutputState(PWM1_BASE, PWM_OUT_4_BIT | PWM_OUT_5_BIT
                       | PWM_OUT_6_BIT | PWM_OUT_7_BIT, true);

    /* Now that the prepping phase is done, we begin the arming process by setting
     * the thrusters to zero, then setting it to a nonzero value, then setting them
     * back to zero again.
     */

    /* This loop is present for the purposes of debugging, it sets each thruster one

     * at a time.
     * Note that we have to scale the input PWM values so that the Tiva is happy
     * with the data we give it, and also note that 1500 is equivalent to "zero"
     * pwm.
     */

    /*The code below is temporary for arming and spinning one thruster*/
    /*
    ROM_PWMPulseWidthSet(PWM1_BASE, pwm_out[0], tiva_pwm_scale(1600));
    vTaskDelay(pdMS_TO_TICKS(1000));
    ROM_PWMPulseWidthSet(PWM1_BASE, pwm_out[0], tiva_pwm_scale(1500));
    vTaskDelay(pdMS_TO_TICKS(2000));

    ROM_PWMPulseWidthSet(PWM1_BASE, pwm_out[0], tiva_pwm_scale(1600));
    */
    /* the above is temporary */

    /* BEGIN ARMING THE THRUSTERS */


    taskENTER_CRITICAL();
    DEBUG_PRINTF("<thruster task>: begin arming process\n");
    for (int i = 0; i < 8; i++) {
        DEBUG_PRINTF("<thruster task>: arming thruster %d\n", i);
        ROM_PWMPulseWidthSet(PWM1_BASE, pwm_out[i], tiva_pwm_scale(1600));
    }
    taskEXIT_CRITICAL();

    vTaskDelay(pdMS_TO_TICKS(1000));

    taskENTER_CRITICAL();
    for (int i = 0; i < 8; i++) {
        ROM_PWMPulseWidthSet(PWM1_BASE, pwm_out[i], tiva_pwm_scale(1500));
    }

    taskEXIT_CRITICAL();

    vTaskDelay(pdMS_TO_TICKS(5000));

    DEBUG_PRINTF("<thruster task>: end arming process\n");

    /* DONE ARMING THRUSTERS */

    /* We enter our infinite loop of continually updating the thruster state,
     * qubobus should be updating the queue with new values from the Jetson as this infinite
     * loop is running.
     */

    //debug_thruster_test(xForceQueue, &test_value);

    DEBUG_PRINTF("<thruster task>: initialized.\n");
    //    xQueueReceive(xForceQueue, &thrus_data, pdMS_TO_TICKS(1000));
    for (;;) {
        if (xQueueReceive(xForceQueue, &thrus_data, pdMS_TO_TICKS(500)) == pdPASS) {
            // ToDo Create the else branch and output_buffer variable
            DEBUG_PRINTF("Raw force values\n");
            DEBUG_PRINTF("%d\n", thrus_data.val[0]);
            DEBUG_PRINTF("%d\n", thrus_data.val[1]);
            DEBUG_PRINTF("%d\n", thrus_data.val[2]);
            DEBUG_PRINTF("%d\n", thrus_data.val[3]);
            DEBUG_PRINTF("%d\n", thrus_data.val[4]);
            DEBUG_PRINTF("%d\n", thrus_data.val[5]);

            matrix_multiply(MATRIX_ROWS, MATRIX_COLS, MATRIX_COLS, 1,
                            jetson_matrix, thrus_data.val, output_buffer);


            /**** NOTE THE 10 BELOW IS TEMPORARY TO TEST FOR COMPILATION *********/
            force_to_pwm(14, output_buffer);

            /* After this, we should have the desired PWM values in the output buffer. We now
             * have to actually apply the data.
             */
            taskENTER_CRITICAL();
            for (int i = 0; i < 8; i++) {
                ROM_PWMPulseWidthSet(PWM1_BASE, pwm_out[i], tiva_pwm_scale((uint32_t)output_buffer[i]));
                DEBUG_PRINTF("<thruster task> (force): set %d to %d\n", i, (int) output_buffer[i]);
            }
            taskEXIT_CRITICAL();
            // Input the values from the queue
            DEBUG_PRINTF("I have left\n");
        } else if (xQueueReceive(xPWMQueue, &thruster_raw, pdMS_TO_TICKS(500)) == pdPASS) {
            thruster_id = ((int *) &thruster_raw)[0];
            pwm = ((int *) &thruster_raw)[1];

            if (thruster_id >= 0 && thruster_id <= 7) {
                ROM_PWMPulseWidthSet(PWM1_BASE, pwm_out[thruster_id], tiva_pwm_scale(pwm));
                //DEBUG_PRINTF("\n<thruster task> (pwm): set %d to %d", thruster_id, pwm);
            }
        } else {
            //DEBUG_PRINTF("nothing in the queue\n");
            //set_def_state();
        }
    }
}
