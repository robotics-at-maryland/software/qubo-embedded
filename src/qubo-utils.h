#if !defined(QUBO_UTILS_H)
#define QUBO_UTILS_H

#ifdef DEBUG
#define DEBUG_PRINTF(...) do{ UARTprintf( __VA_ARGS__ ); } while( false )
#else
/* #define DEBUG_PRINTF(...) do{ } while ( false ) */
#define DEBUG_PRINTF(...)
#endif

#endif
