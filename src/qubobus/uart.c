/*** R@M 2021
 * Alexander Yelovich and Jeffrey Fisher
 */

/**** Module Includes ****/
#include "qubobus.h"
#include "io.h"

#include "cli.h"
#include "qubo-utils.h"

#include <stdbool.h>
#include <stdint.h>

/**** Tivalib Includes ****/
#include <inc/hw_memmap.h>
#include <driverlib/rom.h>
#include <driverlib/pwm.h>
#include <driverlib/uart.h>
#include <driverlib/udma.h>
#include <utils/uartstdio.h>
#include <inc/hw_uart.h>
#include <inc/hw_types.h>

/**** FreeRTOS Includes ****/
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <message_buffer.h>

/**** Private Constants ****/
#define MAX_MESSAGE_SIZE 128
#define MAX_PAYLOAD_SIZE 64

#define QUBOBUS_UART_BASE UART2_BASE

#define DMA_RINGBUF_SIZE (MAX_MESSAGE_SIZE*2)

/* Periods in ms to retry reading messages */
#define INITIALIZE_RETRY_PERIOD 100
#define CHECK_BUFF_PERIOD       20

/**** Private Function Declarations ****/
static bool read_packet_init(void);
static bool read_packet_deinit(void);
static ssize_t uart_read(void *, void *, size_t);
static ssize_t uart_write(void *, void *, size_t);
static ssize_t uart_read_dma(void *io_host, void *buffer, size_t size);
static void UARTIntHandler(void);

static bool dma_initialize(void);
static void dma_deinitialize(void);

/**** Private Variables ****/

/* Handle to refer to this task in the future */
TaskHandle_t read_packet_handle;

static uint8_t dma_ringbuf[DMA_RINGBUF_SIZE];
static uint8_t *ringbuf_readhead = dma_ringbuf;
static uint32_t dma_channel = 0;
static const uint32_t uart_fifo_address = QUBOBUS_UART_BASE + UART_O_DR;

// If n_bytes_written - n_bytes_read > DMA_RINGBUF_SIZE, we're in trouble
static ssize_t n_bytes_read = 0;
static volatile ssize_t n_bytes_written = 0;
static volatile uint32_t last_idx = 0;

/*Private (?) functions for raw read and raw write go here*/
/*Start `raw_io_function`s*/
/* uart_read(): when a notification is received that there are chars to read
 * from UART (which is done from the interrupt handler), we continually check
 * to see if there is a char to read from UART, if there is, then we read in
 * that byte and check again. Once there are no more bytes to be read, or
 * if we have read in up to our max size, then we are good and return the
 * number of bytes read.
 */
static ssize_t uart_read(void *io_host, void *buffer, size_t size) {
    size_t i;
    uint8_t receivedByte;
    uint32_t uartBase = ((uint32_t *)io_host)[0];
    unsigned char ch;

    DEBUG_PRINTF("uart_read: before notification received\n");
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
    DEBUG_PRINTF("uart_read: notification received\n");

    /* non-blocking function */
    for (i = 0; ROM_UARTCharsAvail(uartBase) && i < size; i++) {
        ((uint8_t *) buffer)[i] = (uint8_t) ROM_UARTCharGetNonBlocking(uartBase);
        /* For debug purposes print received characters as we get them. */
        ch = (unsigned char) ((uint8_t *) buffer)[i];
        //DEBUG_PRINTF("uart_read: Received '0x%02x' = '%c'\n", ch, ch);
    }

    return (ssize_t) i; /* i bytes were read */
}

static ssize_t uart_read_dma(void *io_host, void *buffer, size_t size) {
    // Overflow has occurred
    if (n_bytes_written - n_bytes_read > DMA_RINGBUF_SIZE) {
        DEBUG_PRINTF("overflow read: %d written: %d\n", n_bytes_read, n_bytes_written);
        for (;;) { }
        return -1;
    }

    size_t i = 0;
    uint8_t *p_endbuf = dma_ringbuf + DMA_RINGBUF_SIZE - 1;  // last item in buffer

    /* WARNING: Assuming here that we are using the primary control structure */
    uint32_t *dma_control_ptr = (uint32_t *) ROM_uDMAControlBaseGet();
    dma_control_ptr += 4 * dma_channel; // Each control entry is 4 32-bit words
    dma_control_ptr += 2; // The control word is the 3rd word

    /* See page 611 of the tm4c123gh6pm datasheet for explanataion. Grabbing
     * the number of items left and turning that into a buffer index. */
    uint32_t dma_write_idx = (DMA_RINGBUF_SIZE - 1) - ((*dma_control_ptr >> 4) & 0xff);

    /* ENTER ATOMIC SECTION */
    ROM_IntMasterDisable();
    uint32_t diff = dma_write_idx - last_idx;
    n_bytes_written += diff;
    last_idx = dma_write_idx;
    ROM_IntMasterEnable();
    /* EXIT ATOMIC SECTION */

    uint8_t *ringbuf_writehead = dma_ringbuf + dma_write_idx;

    /* Read until we coincide with the write head or until request is filled */
    while (ringbuf_readhead != ringbuf_writehead && i < size) {
        ((uint8_t *)buffer)[i++] = *ringbuf_readhead++;
        if (ringbuf_readhead > p_endbuf) {
            ringbuf_readhead = dma_ringbuf;
        }
    }

    n_bytes_read += i;
    if (n_bytes_written - n_bytes_read > 0) {
        //DEBUG_PRINTF("%d bytes behind\n", n_bytes_written - n_bytes_read);
    }

    return (ssize_t) i;
}

static ssize_t uart_write(void *io_host, void *buffer, size_t size) {
    uint32_t uartBase = ((uint32_t *)io_host)[0];
    uint8_t byteToBeSent = ((uint8_t *)buffer)[0];
    DEBUG_PRINTF("uart_write: Writing '0x%02x' = '%c'\n", byteToBeSent, byteToBeSent);
    /* blocking function */
    ROM_UARTCharPut(uartBase, byteToBeSent);
    return 1; /* one byte was written/sent */
}
/*End `raw_io_function`s*/

/* This interrupt will only be invoked when there is a UART error or the uDMA
 * controller fills up our buffer */
static void UARTIntHandler(void) {
    ROM_IntMasterDisable();

    uint32_t ui32Status = ROM_uDMAIntStatus();
    uint32_t ui32Status_uart = ROM_UARTIntStatus(QUBOBUS_UART_BASE, false);

    // Checking if our channel caused this interrupt, or if is UART related
    if ((ui32Status >> (dma_channel)) & 0x1) {
        //DEBUG_PRINTF("resetting DMA xfer. Total bytes read from UART: %d\n", n_bytes_written);
        n_bytes_written += DMA_RINGBUF_SIZE - last_idx;
        last_idx = 0;
        ROM_uDMAIntClear(0x1 << (dma_channel - 1));
        ROM_uDMAChannelTransferSet(dma_channel | UDMA_PRI_SELECT,
                                   UDMA_MODE_BASIC,
                                   (void *) uart_fifo_address,
                                   dma_ringbuf,
                                   DMA_RINGBUF_SIZE);
        ROM_uDMAChannelEnable(dma_channel);
    }

    UARTIntClear(QUBOBUS_UART_BASE, 0xFFFFFFFF);
    ROM_IntMasterEnable();
}

static bool dma_initialize() {
    /* All of this should only be done once */
    if (QUBOBUS_UART_BASE == UART0_BASE) {
        dma_channel = 8;
        ROM_uDMAChannelAssign(UDMA_CH8_UART0RX);
    } else if (QUBOBUS_UART_BASE == UART2_BASE) {
        dma_channel = 0;
        ROM_uDMAChannelAssign(UDMA_CH0_UART2RX);
        DEBUG_PRINTF("Selecting channel 0\n");
    } else {
        DEBUG_PRINTF("Unsupported UART for this task. Use either UART0 or UART2\n");
        return true;
    }

    /* Set the channel to be high priority, to use the primary control structure,
     * to use bursts, and to accept requests from peripherals */
    ROM_UARTDMAEnable(QUBOBUS_UART_BASE, UART_DMA_RX);

    ROM_uDMAChannelAttributeDisable(dma_channel, UDMA_ATTR_ALTSELECT);
    ROM_uDMAChannelAttributeEnable(dma_channel, UDMA_ATTR_HIGH_PRIORITY |
                                   UDMA_ATTR_REQMASK);

    ROM_uDMAChannelControlSet(dma_channel | UDMA_PRI_SELECT, UDMA_SIZE_8 |
                              UDMA_SRC_INC_NONE |
                              UDMA_DST_INC_8 |
                              UDMA_ARB_4);

    /* This will be done everytime we restart the transfer */
    /* The transfer size should be the number of items - 1 (0 corresponds to 1 item) */
    ROM_uDMAChannelTransferSet(dma_channel | UDMA_PRI_SELECT,
                               UDMA_MODE_BASIC,
                               (void *) uart_fifo_address,
                               dma_ringbuf,
                               DMA_RINGBUF_SIZE);

    /* WARNING: This is harcoded and needs to change */
    HWREG(0x400ff000 + 0x01c) = 0x1 << dma_channel; // Allow our channel to use both bursts and single requests
    HWREG(0x400ff000 + 0x024) = 0x1 << dma_channel; // Allow the peripheral UART to send requests to our channel
    DEBUG_PRINTF("dma useburst clear %08x\n", *((volatile uint32_t *) (0x400ff000 + 0x01c)));
    DEBUG_PRINTF("dma mask request %08x\n", *((volatile uint32_t *) (0x400ff000 + 0x020)));
    DEBUG_PRINTF("dma channel enable %08x\n", *((volatile uint32_t *) (0x400ff000 + 0x028)));
    DEBUG_PRINTF("dma channel alternate %08x\n", *((volatile uint32_t *) (0x400ff000 + 0x510)));

    ROM_uDMAChannelEnable(dma_channel);

    return false;
}

static void dma_deinitialize(void) {
    ROM_uDMAChannelDisable(dma_channel);
}

/**** Public Functions ****/

/*Need to implement prototype somewhere*/
static bool read_packet_init(void) {
    bool retval = false;
    retval = retval || dma_initialize();
    UARTIntRegister(QUBOBUS_UART_BASE, UARTIntHandler);

    return retval;
}

static bool read_packet_deinit(void) {
    dma_deinitialize();
    UARTIntUnregister(QUBOBUS_UART_BASE);

    return false;
}

/* Function to be called by task which reads in a message then assigns
 * it to a queue based on the message type in the packet header.*/
void read_packet(void *params) {
    /*
    As of right now, I think we are going to use a switch statement to
    accomplish this, no queue for the packets I don't think, the TIVA
    is going to be patient and Jetson will determine the frequency at which
    packets are sent, and the TIVA will just have to be ready to receive
    all these packests (maybe this will be changed after going over it
    with Nate.
    */

    /* Since this task is supposed to run indefinitely, we start by
     * initializing our IO_State (using our raw read and write methods)
     * then calling init_connect (either on the Jetson or Tiva). After
     * this, we should enter our infinite loop, which will read a message,
     * then do what was mentioned in the block comment above (using switch
     * statements to verify type of packet, and then assigning data
     * to its appropriate queue.
     */

    /********START OF THE CODE LOGIC*********/
    read_packet_init();

    /* Declare a buffer of some arbitrarily large size to be able to handle
       all types of packets (maybe 64 - 128 bytes depending on what types
       of packets we'll process.
    */
    Message msg_buffer;
    uint8_t payload_buffer[MAX_PAYLOAD_SIZE];
    int8_t wait_error;
    uint32_t io_host[1] = { QUBOBUS_UART_BASE };
    QueueHandle_t xThrusterData = *((QueueHandle_t *) ((void **) params)[0]);

    /* We first initialize the IO_State */
    IO_State state;

    state = initialize(&io_host, &uart_read_dma, &uart_write, 80);
    /* We wait for Jetson to initialize the connection, call wait_connect.
     *
     * If wait_connect returns 0, then we are good
     * If wait_connect returns 1, then the versions of the protocol do not
     * match (and Tiva handles this by sending an error packet to the
       Jetson before returning)
     * If we get -1, then an error occurred
     *   -Tiva could not read an announce message from Jetson
     *   -Tiva could not write a respone message back to Jetson
     *   -Tiva could not read a protocol message from the Jetson
     *   -Tiva could not create a response message to the protocol message
     */
    // Try to connect 5 times per second
    do {
        wait_error = first_wait_connect(&state, &msg_buffer);
        if (wait_error) {
            /*We have an error, address it in a failure function*/
            /* failure(); */
            DEBUG_PRINTF("Error in first_wait_connect()\n");
        } else {
            DEBUG_PRINTF("first_wait_connect SUCCESS\n");
        }

        // Try this at 10 Hz
        vTaskDelay(pdMS_TO_TICKS(INITIALIZE_RETRY_PERIOD));
    } while (wait_error);

    /* We then proceed if wait_connect was successful (returns 0).
     * This is where we enter the infinite loop for the task.
     */

    /****************START INFINITE LOOP**************/

    /* Sit around and wait until we get some possible indication that a
       message has arrived
    */

    int n_mesgs = 0;
    for (;;) {
        if (!read_message(&state, &msg_buffer, payload_buffer)) {
            uint16_t crc = checksum_message(&msg_buffer);
            if (crc != msg_buffer.footer.checksum) {
                DEBUG_PRINTF("CRC Failed!\n");
            } else {
                DEBUG_PRINTF("%d messages\n", ++n_mesgs);
                if (msg_buffer.header.message_id == M_ID_OFFSET_THRUSTER) {
                    DEBUG_PRINTF("read_packet: recognized packet of type M_ID_OFFSET_THRUSTER\n");
                    if (uxQueueSpacesAvailable(xThrusterData) == 0) {
                        xQueueReset(xThrusterData);
                    }

                    xQueueSendToBack(xThrusterData, msg_buffer.payload, portMAX_DELAY);
                } else if (msg_buffer.header.message_type == MT_ANNOUNCE) {
                    /* `wait_connect()` takes two message pointers. We only have
                     * one (`msg_buffer`) so we need to make another one to call
                     * `wait_connect()`. */
                    Message msg2;

                    DEBUG_PRINTF("Received additional announce packet, re-connecting");
                    do {
                        wait_error = wait_connect(&state, &msg_buffer, &msg2);
                        if (wait_error) {
                            /*We have an error, address it in a failure function*/
                            /* failure(); */
                            DEBUG_PRINTF("Error in wait_connect()\n");
                        } else {
                            DEBUG_PRINTF("wait_connect SUCCESS\n");
                        }
                    } while (wait_error);
                } else {
                    DEBUG_PRINTF("There was an error reading the packet\n");
                }
            }
        } else {
            DEBUG_PRINTF("Message failed!\n");
        }
        // TODO: This is sloppy. We need a better way to handle this
        vTaskDelay(pdMS_TO_TICKS(CHECK_BUFF_PERIOD));
    }

}
