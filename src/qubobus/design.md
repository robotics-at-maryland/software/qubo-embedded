
# Motivation
We have the following requirements for the qubobus
- Speed 
- Efficiency
- Reliability

## Speed
At a minimum, we need to keep up with the speed of our UART line. The fastest
we'll be running it is at 115200 baud. 10 baud per byte means 11520 kB per
second.  With a clock rate on the TIVA of 50 MHz this gives us 50,000,000 /
11,520 ~= 4,340 cycles per byte, which should be more than enough for our
system to handle.

Aside from throughput, there is also the question of latency. Qubo is a
real-time system, and we'd like our incoming requests to be serviced as quickly
as possible.

## Efficiency
While we have 4,340 cycles, we certainly can't spend all of them - a lot of the
data we read in will be sent to other parts of the system for processing, each
of which has their own cycle costs. Therefore, there is a precedent for
efficiency. Not only must we keep up with the baudrate of our UART line
(speed), we must do so in the least amount of cycles possible (efficiency).

## Reliability
Lastly, we need reliability. There are a couple of aspects to this:
1. Detection of corrupted data
2. Response to corrupted data
3. Re-entrant operation

### 1. Detection of corrupted data 
Our environment isn't too harsh, and our datarates aren't too high, so we don't
really need ECC on every byte (TODO: If we wanted a less qualitative analysis
of this, how would we go about this?). The original designers of qubobus
impelmented a simple CRC over the packet. When packets arrive, we compute the
checksum and compare against the one stored in the packet. If they don't match,
there was corruption somewhere along the line.

### 2. Response to corrupted data
Upon detection of a bad packet, there are a few actions we can take:

- Do nothing (discard the packet)
- Ask for the packet again

Depending on the contents of the packet, either response may be appropriate.
For example, for system control packets that configure our operation, asking
for the packet again is more appropriate than discarding. For streaming type
packets, such as sensor output or thruster control commands 
(which should be coming frequently), discarding one packet won't hurt.


I think DMA is clearly the way to go in terms of fetching data of the UART.
This can happen in the background, and helps prevent UART FIFO overruns.

The thing about the ring buffer is that we need to check if data is available
in it. The nice thing about the ping-pong solution is we get an interrupt when
data is ready, so we can just yield until that is the case.

The following may be a better solution:
- Use a ping-pong buffer
- Set a watchdog style timer
- On each ping-pong interrupt
    - Reset the watchdog timer
    - Notify our task that data is ready
- If the watchdog timer is not reset, then we cancel the dma transfer and have
  the task read out any data in the partially filled buffer. Restart the
  transfer.

This is an interrupt driven version of the polling we're currently doing for
the ring buffer. We need to make qubobus adjustments to make sure reading part
of a packet (e.g. one straddling the ping-pong) won't be a problem.

Overflow detection is still needed.
