/* Ross Baehr, Nathaniel Renegar
   R@M 2020
   ross.baehr@gmail.com
   naterenegar@gmail.com
   */

// Standard includes
#include <stdbool.h>
#include <stdint.h>

// FreeRTOS
#include <FreeRTOS.h>
#include <queue.h>
#include <task.h>
#include <semphr.h>
#include <message_buffer.h>

// Tiva
#include <inc/hw_memmap.h>
#include <inc/hw_types.h>
#include <inc/hw_nvic.h>
#include <driverlib/gpio.h>
#include <driverlib/i2c.h>
#include <driverlib/pin_map.h>
#include <driverlib/rom.h>
#include <driverlib/sysctl.h>
#include <driverlib/uart.h>
#include <driverlib/fpu.h>

// Module Includes
#include "configure.h"
#include "thrusters.h"
#include "cli.h"
#include "qubobus_task.h"
#include "qubo-utils.h"

// If debug defined, can use this to print to UART
#include <utils/uartstdio.h>

#ifdef DEBUG
void __error__(char *pcFilename, uint32_t ui32Line) {
    for (;;) {
        //blink_rgb(RED_LED, 1);
        DEBUG_PRINTF("ERROR");
    }
}
#endif

void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName ) {
    DEBUG_PRINTF("\nStack Overflow from task %s\n", pcTaskName);
    for (;;) {
    }
}

// Called when a tick interrupt happens
// Can be used to confirm tick interrupt happening
void vApplicationTickHook(void) {
#ifdef DEBUG
    //  DEBUG_PRINTF("\nTick interrupt\n");
#endif
}

static uint8_t udma_control_table[1024] __attribute__  ((aligned (1024)));

int main() {
    /*** Configure the clock and FPU ***/

    // Enable floating point operations
    ROM_FPULazyStackingEnable();
    ROM_FPUEnable();

    // Set the clocking to run at 50 MHz from the PLL
    ROM_SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ |
                       SYSCTL_OSC_MAIN);

    /*** Configure our peripherals ***/
    configureGPIO();
    configurePWM();
    configureUSBPower(); // Right now all this does is set up VBUS
    configureUART();
    configureDMA(udma_control_table);

    DEBUG_PRINTF("<main> : devices configured\n");

    /*** Allocate FreeRTOS data structures for tasks, these are automatically made in heap ***/

    QueueHandle_t *xThrusterData = pvPortMalloc(sizeof(QueueHandle_t));
    *xThrusterData = xQueueCreate(3, sizeof(thrusparam_t));
    if (*xThrusterData == NULL) {
        /*Thruster queue could not be created, do something here*/
    }

    QueueHandle_t *xThrusterRawPWMData = pvPortMalloc(sizeof(QueueHandle_t));
    *xThrusterRawPWMData = xQueueCreate(1, sizeof(uint64_t));
    if (xThrusterRawPWMData == NULL) {
        /* Thruster queue could not be created, do something here */
    }

    /* Master enable interrupts */
    ROM_IntMasterEnable();

    DEBUG_PRINTF("<main> : Datastructures allocated\n");

    /*** Start FreeRTOS tasks ***/
    /*
    QueueHandle_t *thruster_params[2];

    thruster_params[0] = &xThrusterData;
    thruster_params[1] = &xThrusterRawPWMData;
    */
    //QueueHandle_t *thruster_params[1];
    //thruster_params[0] = &xThrusterData;
    void **thruster_params = pvPortMalloc(2 * sizeof(*thruster_params));
    thruster_params[0] = xThrusterData;
    thruster_params[1] = xThrusterRawPWMData;

    /* DEBUG_PRINTF("Starting thrusters task\n"); */
    DEBUG_PRINTF("Starting thrusters task\n");
    if (xTaskCreate(&thruster_task, (const portCHAR *) "Thruster", 256,
                    thruster_params, tskIDLE_PRIORITY + 5, NULL) != pdPASS) {
        DEBUG_PRINTF("Failed in creating thrusters task\n");
        while (1) {}
    }


    void **read_packet_params = pvPortMalloc(1 * sizeof(*read_packet_params));
    read_packet_params[0] = xThrusterData;

    if (xTaskCreate(&read_packet, (const portCHAR *) "Read Message", 256,
                    read_packet_params, tskIDLE_PRIORITY + 4, NULL) != pdPASS) {
        while (1) {}
    }

    /*
    void **CLI_params = pvPortMalloc(2 * sizeof(QueueHandle_t));
    CLI_params[0] = xThrusterData;
    CLI_params[1] = xThrusterRawPWMData;
    if (CLI_task_init(UART2_BASE, CLI_params)) {
        while (1) {}
    }
    */

    vTaskStartScheduler();

    while (1) {
        // Do something, like blink an error RGB
    }
}
