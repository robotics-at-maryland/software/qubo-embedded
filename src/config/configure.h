#ifndef __CONFIGURE_H__
#define __CONFIGURE_H__

/* Nathaniel Renegar
   R@M 2021
   naterenegar@gmail.com
   */

#include <stdint.h>

void configureDMA(uint8_t *p_control_table);
void configureUSB(void);
void configureUSBPower(void);
void configurePWM(void);
void configureUART(void);
void configureGPIO(void);

#endif /* __CONFIGURE_H__ */
