## About
Code that runs on the Tiva, a.k.a. the TI TM4C123GH6PM Microcontroller. This
code talks to most of our sensors, monitors the status of our power system, and
relays sensor information to the main computer. 

## Getting Started
To clone, download dependencies, and build:

```
git clone git@gitlab.com:robotics-at-maryland/software/qubo-embedded.git
scripts/download-toolchain.sh
source scripts/setup-environment.sh
make drivers -B
make
```

## Embedded Programming Tutorials 
This [video series](https://www.youtube.com/playlist?list=PLPW8O6W-1chwyTzI3BHwBLbGQoPFxPAPM)
takes you through embedded programming on the TM4C123GH6PM microcontroller. It
is strongly recommended you watch the first 30 or so of these videos if you
don't have previous experience. If you are familiar with C, you can probably
skip the first few videos. They use the IAR Toolchain and later on a modified
version of Eclispse, which isn't what we use here, so it might be difficult to
follow along.  Regardless, the series is an excellent resource to get an
understanding of embedded programming.

## TODO

- [X] Get rid of global variables as much as possible. This makes it really hard to
  use the codebase
- [X] Refactor our directory structure. Instead of sorting by the kind of file
  (e.g. task definitions, device drivers, interrupts), I think sorting by
  functionality would be a better appraoch. This will make it easier to find
  what you're looking for.
- [ ] Get the TIVA <--> JETSON uart link running
- [X] Refactor the PWM code
- [ ] Port in and test the code that implements the PNI Binary protocol for the AHRS

## Uploading Code
After building an image, run `make flash` to flash the `image.bin` file onto
the tiva from the top level directory. Make sure you have a microusb cable
plugged into the "debug" port of the board, and that the power switch is on the
"debug" setting.

If you are having trouble, a starting point would be `lsusb`. If you don't see
something about "Tiva", "TI", or an In-Circuit Debug Interface, then your
machine isn't seeing the microcontroller.

## Serial/UART
The TM4C123GXL's UART0 is connected to the In-Circuit Debug Interface(ICDI) which you can use the USB
cable to view.

Use a serial terminal program with 115200 bps, 8 data bits, no parity, and 1 stop bit.

## Useful Links
- [Tivaware Drivers](http://software-dl.ti.com/tiva-c/SW-TM4C/latest/index_FDS.html)(_EK-TM4C123GXL_)
- [Tivware Driver Utils User Manual](http://mercury.pr.erau.edu/~siewerts/cec320/documents/Manuals/SDK-PDL/SW-TM4C-UTILS-UG-2.1.4.178.pdf)
- [Tivaware Peripheral Driver User Manual](https://www.ti.com/lit/ug/spmu298e/spmu298e.pdf?ts=1635178261362&ref_url=https%253A%252F%252Fwww.google.com%252F)
- [Tiva Devkit Datasheet](https://www.ti.com/lit/ug/spmu296/spmu296.pdf?ts=1635192465932&ref_url=https%253A%252F%252Fwww.google.ca%252F)
- [Microcontroller Datasheet](https://www.ti.com/lit/ds/spms376e/spms376e.pdf?ts=1635188605832&ref_url=https%253A%252F%252Fwww.ti.com%252Ftool%252FEK-TM4C123GXL%253Futm_source%253Dgoogle%2526utm_medium%253Dcpc%2526utm_campaign%253Depd-null-amcu-TivaTM4C123g-cpc-evm-google-wwe%2526utm_content%253DTivaTM4C123g%2526ds_k%253Dtiva%2Btm4c123g%2526DCM%253Dyes%2526gclid%253DCjwKCAjwq9mLBhB2EiwAuYdMtZFdMgFTZymP8RTkcQySxHjy8iwHSodsPlx3Pjx_7u6bqP4FYm5NCBoCf3gQAvD_BwE%2526gclsrc%253Daw.ds)
- [Tiva Pinout](https://energia.nu/pinmaps/img/EK-TM4C123GXL.jpg)
- [A makefile guide I quite like](https://makefiletutorial.com)

## Dependency Versions
FreeRTOS version: v8.2.3  
Tivaware version: 2.1.3.156
