#!/bin/sh

die() {
    ret=$?
    printf "%s\\n" "$@" >&2
    exit $ret
}

MY_DIR="$(dirname "$0")"
PROJ_ROOT="$MY_DIR/.."
config_dir="$MY_DIR/etc-format"

if ! command -v astyle; then
    die "fatal: Could not find command astyle (Artistic Style)."
fi

if [ ! -d "$config_dir" ]; then
    die "fatal: Directory 'etc-format' not found next to script."
fi
if [ ! -f "$config_dir/astylerc" ]; then
    die "fatal: File '$config_dir/astylerc' not found next to script."
fi

recursive_fmt()
{
    dir="$1"
    if [ ! -d "$dir" ]; then
        die "fatal: Directory '$dir' not found, so could not format it."
    fi

    astyle --project="$config_dir/astylerc" --recursive --suffix='.auto-format-backup' "$dir/*.c" "$dir/*.h"
}

recursive_fmt "$PROJ_ROOT/src"
recursive_fmt "$PROJ_ROOT/qubobus"
recursive_fmt "$PROJ_ROOT/tdd"
