#!/bin/bash

TOOL_PATH=$(git rev-parse --show-toplevel)/tools/
# Fetch the toolchain
wget -P $TOOL_PATH https://developer.arm.com/-/media/Files/downloads/gnu-rm/10.3-2021.07/gcc-arm-none-eabi-10.3-2021.07-x86_64-linux.tar.bz2
bunzip2 ${TOOL_PATH}gcc-arm-none-eabi-10.3-2021.07-x86_64-linux.tar.bz2
tar -xvf ${TOOL_PATH}gcc-arm-none-eabi-10.3-2021.07-x86_64-linux.tar
mv gcc-arm-none-eabi-10.3-2021.07-x86_64-linux ${TOOL_PATH} 
git clone https://github.com/utzig/lm4tools.git ${TOOL_PATH}lm4tools
cd ${TOOL_PATH}lm4tools/lm4flash && make

