#!/bin/bash
TOOL_PATH=$(git rev-parse --show-toplevel)/tools

export PATH=$PATH:"$(realpath $TOOL_PATH/gcc-arm-none-eabi-10.3-2021.07/bin/)"
export PATH=$PATH:"$(realpath $TOOL_PATH/lm4tools/lm4flash)"
